Codigo fuente de la el articulo cientifico: Algoritmo greedy/voraz para predecir el índice de servicio actual de pavimentos basado en agrupación divisiva y regresión lineal
Para obtner los dataset enviar un correo a: pacho328@me.com

Pasos para ejecutar el codigo:

1. Clonar el repositorio [https://gitlab.com/pacho328/agrupacion-greedy-pavimentos](https://gitlab.com/pacho328/agrupacion-greedy-pavimentos)
2. Descargar librerias -> [https://drive.google.com/drive/folders/1dgp7b_gEczyrI8IG86w1VMcATFkGs5Rb?usp=sharing](https://drive.google.com/drive/folders/1dgp7b_gEczyrI8IG86w1VMcATFkGs5Rb?usp=sharing )
3. Abrir netbeans-> abrir proyectó->Ir al directorio local(donde se descargo el repositorio) 
4. Ir a proyectos-> Librerias-> Adicionar JAR->Ir al directorio donde se descargaron las librerias en el paso 2.
5. Correr el proyecto.

Nota: si se desea modificar el ErrorPromedioAceptado y ErrorPromedioAdicional eso parametros se encuentran en el archivo interfaz.java

Nota: importante: solicitar el dataset al correo: pacho328@me.com. 